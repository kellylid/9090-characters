import React, { Component } from 'react';
import './App.css';
import Titulo from './title.gif'

function CampoNome(props) {
  let classesEstilo = "campo-nome "
  let editavel = ""

  if (props.status) {
    classesEstilo += "nome-correto";
    editavel =  "disabled";
  }

  return ( 
    <input type='text' 
      className={classesEstilo} 
      onChange={props.onChange} 
      disabled={editavel} 
    /> 
  );
}

function Resposta(props){
  const estilo = {'textTransform': 'capitalize'};
  return(
    <input type='text' 
      className='campo-nome'
      disabled='disabled'
      value={props.resposta}
      style={estilo}
    />
  );
}

function Imagem(props) {
  const caminho = '/img/' + props.indice + '.jpg';
  return (
    <img src={process.env.PUBLIC_URL + caminho} className="foto" alt={props.indice} />
  );
}

class Tabela extends Component {
  renderPersonagem(i) {
    const status = this.props.pontos.indexOf(i) > -1 ? 1 : 0
    
    if(!this.props.jogando && !status) {
      return(
        <td className="quadro" key={i}>
          <Imagem indice={i} />
          <Resposta 
            resposta={this.props.respostas[i - 1].nome}
          />
        </td>
      )
    }
    else {
      return (
        <td className="quadro" key={i}>
          <Imagem indice={i} />
          <CampoNome 
            key={i}
            onChange={(e) => this.props.onChange(e.target.value, i)} 
            status={status} 
          />
        </td>
      );
    }
  }

  renderTodosPersonagens() {
    let indice = 1;
    const tabela = new Array(15);

    for (let i = 0; i < 15; i++) {
      const linhaPersonagens = new Array(6);

      for (let j = 0; j < 6; j++) {
        const personagem = this.renderPersonagem(indice);
        indice++;
        linhaPersonagens[j] = personagem;
      }

      const linhaElemento = <tr key={i}>{linhaPersonagens}</tr>
      tabela[i] = linhaElemento;
    }

    return (tabela);
  }
  render() {
    const tabela = this.renderTodosPersonagens();
    return (
      <table className="tabela"><tbody>{tabela}</tbody></table>
    );
  }
}

function Painel(props) {
  return (
    <div className="painel-pontos">
      <div className="corretos">
        <span className="qtde-pontos">{props.acertos}</span>
      </div>
      <div className="errados">
        <span className="qtde-pontos">{90 - props.acertos}</span>
      </div>
      <button onClick={props.onClick}>Desisto!</button>
    </div>
  );
}

class Jogo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pontos: [],
      jogando: true,
      respostas: require('./characters.json'),
    };
  }

  digitou = (nomeDigitado, indice) => {
    if (this.state.jogando && nomeDigitado.toLowerCase() === this.state.respostas[indice - 1].nome) {
        const listaPontosAux = this.state.pontos;
        listaPontosAux.push(indice);

        this.setState({
          pontos: listaPontosAux,
        });

        if (listaPontosAux.length === 90) {
          alert('Parabéns! Claramente você assistiu muito desenho animado!');
          this.setState({
            jogando: false,
          });
        }
      }
  }

  desistiu = () => {
    this.setState({
      jogando: false,
    });
  }

  render() {
    return (
      <div className="App">
        <header>
          <img src={Titulo} className="titulo" alt="titulo"/>
          <Painel 
            acertos={this.state.pontos.length} 
            onClick={this.desistiu} 
          />
        </header>
        <Tabela 
          pontos={this.state.pontos} 
          onChange={this.digitou} 
          jogando={this.state.jogando}
          respostas={this.state.respostas}
        />
      </div>
    );
  }
}
export default Jogo;

import React, { Component } from 'react';
import './App.css';
import Titulo from './title.gif'

function CampoNome() {
  return ( <input type='text' className="campo-nome" /> );
}

function Resposta(){
  const estilo = {'textTransform': 'capitalize'};
  return( <input type='text' className='campo-nome' disabled='disabled' style={estilo} />);
}

function Imagem(props) {
  const caminho = '/img/' + props.indice + '.jpg';
  return (
    <img src={process.env.PUBLIC_URL + caminho} className="foto" alt={props.indice} />
  );
}

class Tabela extends Component {
  renderPersonagem(i) {
    return (
      <td className="quadro" key={i}>
        <Imagem indice={i} />
        <CampoNome key={i} />
      </td>
    );
  }

  renderTodosPersonagens() {
    let indice = 1;
    const tabela = new Array(15);

    for (let i = 0; i < 15; i++) {
      const linhaPersonagens = new Array(6);

      for (let j = 0; j < 6; j++) {
        const personagem = this.renderPersonagem(indice);
        indice++;
        linhaPersonagens[j] = personagem;
      }

      const linhaElemento = <tr key={i}>{linhaPersonagens}</tr>
      tabela[i] = linhaElemento;
    }

    return (tabela);
  }
  render() {
    const tabela = this.renderTodosPersonagens();
    return (
      <table className="tabela"><tbody>{tabela}</tbody></table>
    );
  }
}

function Painel() {
  return (
    <div className="painel-pontos">
      <div className="corretos">
        <span className="qtde-pontos">0</span>
      </div>
      <div className="errados">
        <span className="qtde-pontos">90</span>
      </div>
      {/* <button>Desisto!</button> */}
    </div>
  );
}

class Jogo extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div className="App">
        <header>
          <img src={Titulo} className="titulo" alt="titulo"/>
          <Painel />
        </header>
        <Tabela />
      </div>
    );
  }
}
export default Jogo;
